pipeline {
    agent any

    tools {
        // Install the Maven version configured as "M3" and add it to the path.
        maven "maven 3.6.3"
    }

    environment {
        // Credentials에 설정된, gitlab 저장소에 접근가능한 계정
        GIT_CREDENTIAL_ID = 'haseok86'
        RUNDECK_PROJECT_ROOT = '/target/'
    }

    parameters {
        string(name: 'projectName', defaultValue: 'springboot', description: '프로젝트명 (Rundeck상의 프로젝트 명칭과 동일하게 사용(대소문자 구분)')
        string(name: 'fileExtension', defaultValue: 'jar', description: 'war 사용시 변경 필요')

        string(name: 'gitUrl', defaultValue: 'git@bitbucket.org:Haseok/springbootdemo.git', description: 'https 형식의 저장소 url (git 프로토콜 사용불가)')
        choice(name: 'checkoutType', choices: ['branch', 'tag'], description: '빌드할 브랜치')
        string(name: 'branchName', defaultValue: 'master', description: 'branch name / tag name')
    }


    stages {


        stage('Build') {
            steps {
                echo "Generate build file prefix and git target"

                echo "git from : ${branchName}"

                git branch: branchName,
                        credentialsId: env.GIT_CREDENTIAL_ID,
                        url: gitUrl

                echo "Maven build start"
                sh "mvn clean install -DskipTests"
            }
        }


        stage('Add Build Info') {
            steps {
                echo 'Add timestamp and build number to the file'
                script{
                    build_filename = sh(returnStdout: true, script: "ls ./target/ | grep -E '.${fileExtension}\$'").trim()
                    final_filename = new Date().format('yyyy_MMdd_HHmm') + "_no" + env.BUILD_NUMBER + "_" + build_filename

                    sh "cp ./target/${build_filename} ./${final_filename}"
                    sh 'ls -al'
                }
            }
        }



        stage('Copy to Rundeck') {
            steps {
                echo 'Copy the file to Rundeck project path'
                script{
                    rundeck_project_path = RUNDECK_PROJECT_ROOT + projectName
                    sh "/bin/cp ./${final_filename} ${rundeck_project_path}/${final_filename}"
                }
            }
        }

    }



    post {
        success {
            echo "Succeed : ${final_filename}"
        }
        failure {
            echo 'Failure! Please check console log.'
        }
    }
}
