package com.baeggu.demopipeline;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PipeController {

    @GetMapping("/good")
    public String good() {
        return "good";
    }
}
